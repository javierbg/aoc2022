import click
from typing import TextIO, Tuple, Union, Literal
import numpy as np
from numpy.typing import NDArray


EMPTY = 0
OPEN = 1
WALL = 2

character_correspondence = {
    ' ': EMPTY, '.': OPEN, '#': WALL
}

ForwardMove = int
FacingMove = Union[Literal['L'], Literal['R']]
Move = Union[ForwardMove, FacingMove]
Coord = Tuple[int, int]
Facing = Union[Literal['L'], Literal['R'], Literal['U'], Literal['D']]
Axis = Union[Literal[0], Literal[1]]

FACING_DIRECTION: dict[Facing, NDArray] = {
    'L': np.array([ 0, -1]),
    'R': np.array([ 0,  1]),
    'U': np.array([-1,  0]),
    'D': np.array([ 1,  0]),
}

FACING_AXIS: dict[Facing, Axis] = {
    'L': 1, 'R': 1,
    'U': 0, 'D': 0
}

FACING_MOVE: dict[Facing, dict[FacingMove, Facing]] = {
    'L': {'L': 'D', 'R': 'U'},
    'R': {'L': 'U', 'R': 'D'},
    'U': {'L': 'L', 'R': 'R'},
    'D': {'L': 'R', 'R': 'L'},
}


def parse_file(input_file: TextIO) -> Tuple[NDArray, list[Move]]:
    char_rows: list[str] = list()

    for line in input_file:
        line = line[:-1]
        if not line:
            break

        char_rows.append(line)

    n_columns = max(map(len, char_rows))
    char_rows = list(map(lambda s: s.ljust(n_columns), char_rows))
    int_rows: list[list[int]] = [list(map(lambda c: character_correspondence[c], row)) for row in char_rows]
    puzzle_map = np.array(int_rows)
    puzzle_map = np.pad(puzzle_map, (1, 1), 'constant', constant_values=EMPTY)

    moves_string = input_file.read()[:-1]
    pointer_begin = 0
    pointer_end = 0
    moves = list()
    while pointer_begin < len(moves_string):
        if moves_string[pointer_begin] in ('L', 'R'):
            moves.append(moves_string[pointer_begin])
            pointer_begin += 1
            pointer_end = pointer_begin
        elif (pointer_end == len(moves_string)) or (not moves_string[pointer_end].isdigit()):
            moves.append(int(moves_string[pointer_begin:pointer_end]))
            pointer_begin = pointer_end
        else:
            pointer_end += 1

    return puzzle_map, moves


def starting_position(puzzle_map: NDArray) -> Tuple[Coord, Facing]:
    starting_column = np.argmax(puzzle_map[1, :] == OPEN).astype(int)
    return (1, starting_column), 'R'


def traverse(puzzle_map: NDArray, moves: list[Move], position: Coord, facing: Facing, verbose: bool) -> Tuple[Coord, Facing]:
    for move in moves:
        if verbose:
            print(position, facing, move)

        if isinstance(move, str):
            facing = FACING_MOVE[facing][move]
            continue

        if FACING_AXIS[facing] == 0:
            line = puzzle_map[:, position[1]]
            looking_up = line[position[0]::-1]
            looking_down = line[position[0]:]
            span_from = (position[0]  - (np.argmax(looking_up == EMPTY) - 1)).astype(int)
            span_to = (position[0] + np.argmax(looking_down == EMPTY)).astype(int)
            looking_ahead = np.roll(line[span_from:span_to], position[0] - span_from)
            if facing == 'D':
                looking_ahead = np.roll(line[span_from:span_to], span_from - position[0] - 1)
                if not (looking_ahead[:move] == OPEN).all():
                    move = np.argmax(looking_ahead == WALL)
                position = (((position[0] - span_from + move) % len(looking_ahead)) + span_from, position[1])
            else:
                looking_ahead = np.roll(np.flip(line[span_from:span_to]), position[0] - span_from)
                if not (looking_ahead[:move] == OPEN).all():
                    move = np.argmax(looking_ahead == WALL)
                position = (((position[0] - span_from - move) % len(looking_ahead)) + span_from, position[1])
        else:
            line = puzzle_map[position[0], :]
            looking_left = line[position[1]::-1]
            looking_right = line[position[1]:]
            span_from = (position[1] - (np.argmax(looking_left == EMPTY) - 1)).astype(int)
            span_to = (position[1] + np.argmax(looking_right == EMPTY)).astype(int)
            if facing == 'R':
                looking_ahead = np.roll(line[span_from:span_to], span_from - position[1] - 1)
                if not (looking_ahead[:move] == OPEN).all():
                    move = np.argmax(looking_ahead == WALL)
                position = (position[0], ((position[1] - span_from + move) % len(looking_ahead)) + span_from)
            else:
                looking_ahead = np.roll(np.flip(line[span_from:span_to]), position[1] - span_from)
                if not (looking_ahead[:move] == OPEN).all():
                    move = np.argmax(looking_ahead == WALL)
                position = (position[0], ((position[1] - span_from - move) % len(looking_ahead)) + span_from)
    
    if verbose:
        print(position)
    return position, facing


FACING_NUMBER: dict[Facing, int] = {
    'R': 0,
    'D': 1,
    'L': 2,
    'U': 3
}
def compute_answer(final_position: Coord, final_facing: Facing) -> int:
    return 1000 * final_position[0] + 4 * final_position[1] + FACING_NUMBER[final_facing]


@click.command()
@click.argument('input_file', type=click.File('r'))
@click.option('-v', '--verbose', is_flag=True)
def main(input_file: TextIO, verbose: bool):
    puzzle_map, moves = parse_file(input_file)
    initial_position, initial_facing = starting_position(puzzle_map)
    final_position, final_facing = traverse(puzzle_map, moves, initial_position, initial_facing, verbose)
    print(compute_answer(final_position, final_facing))


if __name__ == '__main__':
    main()
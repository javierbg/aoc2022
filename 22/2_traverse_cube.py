import click
from typing import TextIO, Tuple, Union, Literal, Optional
import numpy as np
from numpy.typing import NDArray


EMPTY = 0
OPEN = 1
WALL = 2

character_correspondence = {
    ' ': EMPTY, '.': OPEN, '#': WALL
}

ForwardMove = int
FacingMove = Union[Literal['L'], Literal['R']]
Move = Union[ForwardMove, FacingMove]
Coord = Tuple[int, int]
Facing = Union[Literal['L'], Literal['R'], Literal['U'], Literal['D']]

FACING_DIRECTION: dict[Facing, Tuple[int, int]] = {
    'L': (0, -1),
    'R': (0, 1),
    'U': (-1, 0),
    'D': (1, 0)
}

FACING_MOVE: dict[Facing, dict[FacingMove, Facing]] = {
    'L': {'L': 'D', 'R': 'U'},
    'R': {'L': 'U', 'R': 'D'},
    'U': {'L': 'L', 'R': 'R'},
    'D': {'L': 'R', 'R': 'L'},
}


def parse_file(input_file: TextIO) -> Tuple[NDArray, list[Move]]:
    char_rows: list[str] = list()

    for line in input_file:
        line = line[:-1]
        if not line:
            break

        char_rows.append(line)

    n_columns = max(map(len, char_rows))
    char_rows = list(map(lambda s: s.ljust(n_columns), char_rows))
    int_rows: list[list[int]] = [list(map(lambda c: character_correspondence[c], row)) for row in char_rows]
    puzzle_map = np.array(int_rows)

    moves_string = input_file.read()[:-1]
    pointer_begin = 0
    pointer_end = 0
    moves = list()
    while pointer_begin < len(moves_string):
        if moves_string[pointer_begin] in ('L', 'R'):
            moves.append(moves_string[pointer_begin])
            pointer_begin += 1
            pointer_end = pointer_begin
        elif (pointer_end == len(moves_string)) or (not moves_string[pointer_end].isdigit()):
            moves.append(int(moves_string[pointer_begin:pointer_end]))
            pointer_begin = pointer_end
        else:
            pointer_end += 1

    return puzzle_map, moves

FACE_CORNERS = {
    1: (0, 50),
    2: (0, 100),
    3: (50, 50),
    4: (100, 0),
    5: (100, 50),
    6: (150, 0),
}

def step(puzzle_map: NDArray, position: Coord, facing: Facing) -> Optional[Tuple[Coord, Facing]]:
    in_face = None
    for face, (row, col) in FACE_CORNERS.items():
        if (row <= position[0] < (row+50)) and (col <= position[1] < (col+50)):
            in_face = face
            break
    assert in_face is not None

    row, col = position

    if in_face == 1:
        if (row == 0) and (facing == 'U'):
            next_potential_pos = (150 + col - 50, 0)
            next_potential_facing = 'R'
        elif (col == 50) and (facing == 'L'):
            next_potential_pos = (149 - row, 0)
            next_potential_facing = 'R'
        else:
            direction = FACING_DIRECTION[facing]
            next_potential_pos = (row + direction[0], col + direction[1])
            next_potential_facing = facing

    elif in_face == 2:
        if (row == 0) and (facing == 'U'):
            next_potential_pos = (199, col - 100)
            next_potential_facing = 'U'
        elif (row == 49) and (facing == 'D'):
            next_potential_pos = (50 + (col - 100), 99)
            next_potential_facing = 'L'
        elif (col == 149) and (facing == 'R'):
            next_potential_pos = (149 - row, 99)
            next_potential_facing = 'L'
        else:
            direction = FACING_DIRECTION[facing]
            next_potential_pos = (row + direction[0], col + direction[1])
            next_potential_facing = facing

    elif in_face == 3:
        if (col == 50) and (facing == 'L'):
            next_potential_pos = (100, row - 50)
            next_potential_facing = 'D'
        elif (col == 99) and (facing == 'R'):
            next_potential_pos = (49, 100 + (row - 50))
            next_potential_facing = 'U'
        else:
            direction = FACING_DIRECTION[facing]
            next_potential_pos = (row + direction[0], col + direction[1])
            next_potential_facing = facing

    elif in_face == 4:
        if (row == 100) and (facing == 'U'):
            next_potential_pos = (50 + col, 50)
            next_potential_facing = 'R'
        elif (col == 0) and (facing == 'L'):
            next_potential_pos = (49 - (row - 100), 50)
            next_potential_facing = 'R'
        else:
            direction = FACING_DIRECTION[facing]
            next_potential_pos = (row + direction[0], col + direction[1])
            next_potential_facing = facing

    elif in_face == 5:
        if (col == 99) and (facing == 'R'):
            next_potential_pos = (149 - row, 149)
            next_potential_facing = 'L'
        elif (row == 149) and (facing == 'D'):
            next_potential_pos = (150 + (col - 50), 49)
            next_potential_facing = 'L'
        else:
            direction = FACING_DIRECTION[facing]
            next_potential_pos = (row + direction[0], col + direction[1])
            next_potential_facing = facing

    elif in_face == 6:
        if (col == 0) and (facing == 'L'):
            next_potential_pos = (0, 50 + (row - 150))
            next_potential_facing = 'D'
        elif (col == 49) and (facing == 'R'):
            next_potential_pos = (149, 50 + (row - 150))
            next_potential_facing = 'U'
            ####
        elif (row == 199) and (facing == 'D'):
            next_potential_pos = (0, 100 + col)
            next_potential_facing = 'D'
        else:
            direction = FACING_DIRECTION[facing]
            next_potential_pos = (row+direction[0], col+direction[1])
            next_potential_facing = facing

    else:
        raise RuntimeError()

    if puzzle_map[next_potential_pos[0], next_potential_pos[1]] == OPEN:
        return next_potential_pos, next_potential_facing
    else:
        return None

def starting_position(puzzle_map: NDArray) -> Tuple[Coord, Facing]:
    starting_column = np.argmax(puzzle_map[0, :] == OPEN).astype(int)
    return (0, starting_column), 'R'


def traverse(puzzle_map: NDArray, moves: list[Move], position: Coord, facing: Facing, verbose: bool) -> Tuple[Coord, Facing]:
    for move in moves:
        if verbose:
            print(position, facing, move)

        if isinstance(move, str):
            facing = FACING_MOVE[facing][move]
            continue

        for i in range(move):
            if (r := step(puzzle_map, position, facing)) is not None:
                position, facing = r
            else:
                break

    return position, facing



FACING_NUMBER: dict[Facing, int] = {
    'R': 0,
    'D': 1,
    'L': 2,
    'U': 3
}
def compute_answer(final_position: Coord, final_facing: Facing) -> int:
    return 1000 * (final_position[0] + 1) + 4 * (final_position[1] + 1) + FACING_NUMBER[final_facing]


@click.command()
@click.argument('input_file', type=click.File('r'))
@click.option('-v', '--verbose', is_flag=True)
def main(input_file: TextIO, verbose: bool):
    puzzle_map, moves = parse_file(input_file)
    initial_position, initial_facing = starting_position(puzzle_map)
    final_position, final_facing = traverse(puzzle_map, moves, initial_position, initial_facing, verbose)
    print(compute_answer(final_position, final_facing))


if __name__ == '__main__':
    main()
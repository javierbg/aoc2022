import click
from typing import TextIO
import re
from dataclasses import dataclass
import numpy as np
from numpy.typing import NDArray
from itertools import product
from pprint import pprint


@dataclass
class Node:
    name: str
    flow_rate: int
    tunnels: set[str]


def shortest_distance_matrix(ordered_nodes: list[Node], node_indexes: dict[str, int]) -> NDArray:
    n_nodes = len(ordered_nodes)
    
    dist = np.full((n_nodes, n_nodes), -1, dtype=int)
    np.fill_diagonal(dist, 0)
    for i, node in enumerate(ordered_nodes):
        for tunnel in node.tunnels:
            dist[i, node_indexes[tunnel]] = 1

    m = lambda v: np.inf if v == -1 else v

    for k, i, j in product(range(n_nodes), range(n_nodes), range(n_nodes)):
        if m(dist[i, j]) > (m(dist[i, k]) + m(dist[k, j])):
            dist[i, j] = dist[i, k] + dist[k, j]

    return dist


def parse_nodes(input_file: TextIO) -> list[Node]:
    node_list: list[Node] = list()
    for line in input_file:
        m = re.match(r'Valve (?P<node_name>\S+) has flow rate=(?P<flow_rate>\d+); tunnels? leads? to valves?\s+(?P<tunnels>.+)', line[:-1])
        assert m is not None

        node_list.append(Node(
            m.group('node_name'),
            int(m.group('flow_rate')),
            set(m.group('tunnels').split(', '))
        ))

    return sorted(node_list, key=lambda n: n.name)


def best_possible_score(nodes: list[Node], distance: NDArray):
    def search(best_score: int, nodes_left: set[int], minutes_left: int, solution: list[int], score: int) -> int:
        for next_node in nodes_left:
            minutes_after_next = minutes_left - distance[solution[-1], next_node] - 1
            
            if minutes_after_next <= 0:
                continue

            new_score = score + minutes_after_next * nodes[next_node].flow_rate
            if new_score > best_score:
                best_score = new_score

            new_nodes_left = nodes_left - {next_node}

            best_score_bound = new_score
            hypothetical_next_minutes_left = minutes_after_next
            for fr in sorted((nodes[i].flow_rate for i in new_nodes_left), reverse=True):
                hypothetical_next_minutes_left -= 2  # go to next, pull lever
                if hypothetical_next_minutes_left <= 0:
                    break
                best_score_bound += fr * hypothetical_next_minutes_left

            if best_score_bound <= best_score:
                continue
            
            new_solution = solution + [next_node]
            best_score = search(best_score, new_nodes_left, minutes_after_next, new_solution, new_score)
        
        return best_score

    # Starting node is always 0 (AA)
    candidate_valves = {i for i in range(1, len(nodes)) if nodes[i].flow_rate > 0}
    return search(0, candidate_valves, 30, [0], 0)


@click.command()
@click.argument('input_file', type=click.File('r'))
def main(input_file: TextIO):
    node_list = parse_nodes(input_file)
    node_indexes = {node.name: i for i, node in enumerate(node_list)}
    distances = shortest_distance_matrix(node_list, node_indexes)

    print(best_possible_score(node_list, distances))


if __name__ == '__main__':
    main()
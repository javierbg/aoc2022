from collections import defaultdict
from itertools import repeat
import click
from typing import TextIO

Coord = tuple[int, int]

def parse_file(input_file: TextIO) -> list[Coord]:
    elves = list()
    for row, line in enumerate(input_file):
        for col, character in enumerate(line[:-1]):
            if character == '#':
                elves.append((row, col))
    return elves

CHECKLIST: list[tuple[tuple[Coord, Coord, Coord], Coord]] = [
    (((-1, -1), (-1,  0), (-1,  1)), (-1,  0)), # check and move north
    ((( 1, -1), ( 1,  0), ( 1,  1)), ( 1,  0)), # check and move south
    (((-1, -1), ( 0, -1), ( 1, -1)), ( 0, -1)), # check and move west
    (((-1,  1), ( 0,  1), ( 1,  1)), ( 0,  1)), # check and move east
]

NEIGHBOURS: list[Coord] = [
    (-1, -1),
    (-1, 0),
    (-1, 1),
    (0, -1),
    (0, 1),
    (1, -1),
    (1, 0),
    (1, 1),
]

def step(elf_positions: list[Coord], elf_positions_set: set[Coord], checklist_begin: int) -> bool:
    target_positions: dict[Coord, list[int]] = defaultdict(lambda: list())
    for elf_index, (er, ec) in enumerate(elf_positions):
        if all((er+nr, ec+nc) not in elf_positions_set for nr, nc in NEIGHBOURS):
            continue

        for checklist_index in range(4):
            check_positions, (mr, mc) = CHECKLIST[(checklist_begin + checklist_index) % 4]
            if all((er+cr, ec+cc) not in elf_positions_set for cr, cc in check_positions):
                target_positions[(er+mr, ec+mc)].append(elf_index)
                break

    any_moved = False
    for target_position, candidate_elves in target_positions.items():
        if len(candidate_elves) > 1:
            continue
        moving_elf = candidate_elves[0]
        previous_position = elf_positions[moving_elf]
        elf_positions[moving_elf] = target_position

        elf_positions_set.remove(previous_position)
        elf_positions_set.add(target_position)

        any_moved = True
    
    return any_moved


def simulate(elf_positions: list[Coord], n_rounds: int | None, verbose: bool) -> int:
    real_n_rounds = 0
    positions_set = set(elf_positions)

    checklist_begin = 0

    if isinstance(n_rounds, int):
        it = range(n_rounds)
    else:
        it = repeat(None)

    for _ in it:
        if verbose:
            print_field(elf_positions)
            input()
        any_moved = step(elf_positions, positions_set, checklist_begin)
        real_n_rounds += 1
        checklist_begin = (checklist_begin + 1) % 4

        if not any_moved:
            break

    return real_n_rounds


def print_field(elf_positions: list[Coord]):
    min_row = min(r for r, _ in elf_positions)
    max_row = max(r for r, _ in elf_positions)

    min_col = min(c for _, c in elf_positions)
    max_col = max(c for _, c in elf_positions)

    field_height = max_row - min_row + 1
    field_widht = max_col - min_col + 1

    field = [['.' for _ in range(field_widht)] for _ in range(field_height)]

    for row, col in elf_positions:
        field[row - min_row][col - min_col] = '#'

    print('\n'.join(''.join(row) for row in field))

def empty_field(elf_positions: list[Coord]) -> int:
    min_row = min(r for r, _ in elf_positions)
    max_row = max(r for r, _ in elf_positions)

    min_col = min(c for _, c in elf_positions)
    max_col = max(c for _, c in elf_positions)

    field_height = max_row - min_row + 1
    field_widht = max_col - min_col + 1

    return (field_height * field_widht) - len(elf_positions)

@click.command()
@click.argument('input_file', type=click.File('r'))
@click.option('-v', '--verbose', is_flag=True)
def main(input_file: TextIO, verbose: bool):
    elves = parse_file(input_file)
    print(simulate(elves, None, verbose))


if __name__ == '__main__':
    main()
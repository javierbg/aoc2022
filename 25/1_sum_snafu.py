from itertools import zip_longest
from functools import reduce
import click
from typing import TextIO, Literal, cast

SNAFUDigit = Literal[-2, -1, 0, 1, 2]
SNAFUNumber = list[SNAFUDigit]
DIGITS: dict[str, SNAFUDigit] = {
    '0': 0,
    '1': 1,
    '2': 2,
    '-': -1,
    '=': -2,
}
INVERSE_DIGITS: dict[SNAFUDigit, str] = {
    0: '0',
    1: '1',
    2: '2',
    -1: '-',
    -2: '=',
}

def parse_file(input_file: TextIO) -> list[SNAFUNumber]:
    return [
        list(reversed([DIGITS[d] for d in line[:-1]]))
        for line in input_file
    ]


def add_snafu(a: SNAFUNumber, b: SNAFUNumber) -> SNAFUNumber:
    result: SNAFUNumber = [0 for _ in range(max(len(a), len(b))+1)]
    for power, (da, db) in enumerate(zip_longest(a, b, fillvalue=0)):
        r = da + db + result[power]
        
        if r > 2:
            result[power+1] = cast(SNAFUDigit, result[power+1] + 1)
        elif r < -2:
            result[power+1] = cast(SNAFUDigit, result[power+1] - 1)

        result[power] = cast(SNAFUDigit, ((r + 2) % 5) - 2)

    if result[-1] == 0:
        return result[:-1]
    else:
        return result


def snafu_repr(n: SNAFUNumber) -> str:
    return ''.join(map(lambda d: INVERSE_DIGITS[d], reversed(n)))


@click.command()
@click.argument('input_file', type=click.File('r'))
def main(input_file: TextIO):
    snafu_number_list = parse_file(input_file)
    snafu_total = reduce(add_snafu, snafu_number_list, [])
    print(snafu_repr(snafu_total))


if __name__ == '__main__':
    main()
n_overlap = 0
while True:
    try:
        range_x_str, range_y_str = input().split(',')

        range_x_bounds = tuple(map(int, range_x_str.split('-')))
        range_y_bounds = tuple(map(int, range_y_str.split('-')))

        range_x_set = set(range(range_x_bounds[0], range_x_bounds[1]+1))
        range_y_set = set(range(range_y_bounds[0], range_y_bounds[1]+1))

        if range_x_set & range_y_set:
            n_overlap += 1
    except EOFError:
        break

print(n_overlap)
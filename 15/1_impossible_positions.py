import click
from typing import TextIO, Tuple
import re
from pprint import pprint


Coord = Tuple[int, int]

def parse_line(line: str) -> Tuple[Coord, Coord]:
    m = re.match(r'Sensor at x=(?P<sensor_x>[-+]?\d+), y=(?P<sensor_y>[-+]?\d+): closest beacon is at x=(?P<beacon_x>[-+]?\d+), y=(?P<beacon_y>[-+]?\d+)', line[:-1])
    assert m is not None
    return ((int(m.group('sensor_x')), int(m.group('sensor_y'))), (int(m.group('beacon_x')), int(m.group('beacon_y'))))


def distance(ca: Coord, cb: Coord) -> int:
    return abs(ca[0] - cb[0]) + abs(ca[1] - cb[1])


@click.command()
@click.argument('input_file', type=click.File('r'))
@click.option('-r', '--row', 'inspecting_row', type=int, default=2000000)
def main(input_file: TextIO, inspecting_row: int):
    sensors_and_closest_beacons = [parse_line(line) for line in input_file]
    distance_to_beacon = {s: distance(s, b) for s, b in sensors_and_closest_beacons}

    beacons_x_in_inspected_row = {bx for _, (bx, by) in sensors_and_closest_beacons if by == inspecting_row}
    sensors_x_in_inspected_row = {sx for (sx, sy), _ in sensors_and_closest_beacons if sy == inspecting_row}

    impossible_positions = set()
    for (sx, sy), _ in sensors_and_closest_beacons:
        y_distance = abs(inspecting_row - sy)
        sensor_distance_to_beacon = distance_to_beacon[(sx, sy)]
        discriminant = sensor_distance_to_beacon - y_distance
        if discriminant < 0:
            continue

        x_bound_a = sx - discriminant
        x_bound_b = sx + discriminant
        left_x_bound  = min(x_bound_a, x_bound_b)
        right_x_bound = max(x_bound_a, x_bound_b)

        impossible_positions |= set(range(left_x_bound, right_x_bound+1))
    impossible_positions = impossible_positions - beacons_x_in_inspected_row - sensors_x_in_inspected_row

    print(len(impossible_positions))


if __name__ == '__main__':
    main()
from itertools import chain
import click
from typing import TextIO, Tuple, cast
import re
from pprint import pprint
from tqdm import trange


Coord = Tuple[int, int]

def parse_line(line: str) -> Tuple[Coord, Coord]:
    m = re.match(r'Sensor at x=(?P<sensor_x>[-+]?\d+), y=(?P<sensor_y>[-+]?\d+): closest beacon is at x=(?P<beacon_x>[-+]?\d+), y=(?P<beacon_y>[-+]?\d+)', line[:-1])
    assert m is not None
    return ((int(m.group('sensor_x')), int(m.group('sensor_y'))), (int(m.group('beacon_x')), int(m.group('beacon_y'))))


def distance(ca: Coord, cb: Coord) -> int:
    return abs(ca[0] - cb[0]) + abs(ca[1] - cb[1])

class ColumnRange:
    def __init__(self, start: int, end: int) -> None:
        self.start = min(start, end)
        self.end = max(start, end)

    def is_in_range(self, col: int) -> bool:
        return self.start <= col <= self.end

    def is_superset_of(self, other) -> bool:
        return (self.is_in_range(other.start) and self.is_in_range(other.end))

    def intersects(self, other) -> bool:
        return (self.is_in_range(other.start)) or (self.is_in_range(other.end))  or \
               (other.is_in_range(self.start)) or (other.is_in_range(self.end))

    def touches(self, other: 'ColumnRange') -> bool:
        return self.intersects(other)  or \
               (self.end == (other.start + 1)) or (self.start == (other.end + 1)) or \
               (other.end == (self.start + 1)) or (other.start == (self.end + 1))

    def union(self, other: 'ColumnRange') -> 'ColumnRange':
        assert self.touches(other)

        if self.is_superset_of(other):
            return self
        
        if other.is_superset_of(self):
            return other

        return ColumnRange(min(self.start, other.start), max(self.end, other.end))

    def __lt__(self, other) -> bool:
        assert not self.touches(other)
        return self.end < other.start

    def __str__(self) -> str:
        return f'[{self.start}, {self.end}]'

class ColumnRangeSet:
    def __init__(self) -> None:
        self.sorted_ranges: list[ColumnRange] = list()

    def add(self, new_range: ColumnRange):
        first_touching = None
        should_go_before = None

        for i, ith_range in enumerate(self.sorted_ranges):
            they_touch = ith_range.touches(new_range)

            if they_touch:
                first_touching = i
                break

            if new_range < ith_range:
                should_go_before = i
                break
        else:
            # goes at the end
            self.sorted_ranges.append(new_range)
            return

        if first_touching is not None:
            # find any other intersections
            for j, ith_range in enumerate(self.sorted_ranges[first_touching+1:], start=first_touching+1):
                if not ith_range.touches(new_range):
                    last_touching = j # not inclusive
                    break
            else:
                last_touching = len(self.sorted_ranges)
            
            new_union = new_range
            for i in range(first_touching, last_touching):
                new_union = new_union.union(self.sorted_ranges[i])
            
            self.sorted_ranges = self.sorted_ranges[:first_touching] + [new_union] + self.sorted_ranges[last_touching:]

        if should_go_before is not None:
            self.sorted_ranges = self.sorted_ranges[:should_go_before] + [new_range] + self.sorted_ranges[should_go_before:]

    def __str__(self) -> str:
        return ' - '.join(map(str, self.sorted_ranges))
        

@click.command()
@click.argument('input_file', type=click.File('r'))
@click.option('-b', '--coordinate-bounds', nargs=2, type=int, default=(0, 4000000))
@click.option('-r', '--row-range', nargs=2, type=int, default=(0, 4000000))
@click.option('-p', '--progress', is_flag=True)
def main(input_file: TextIO, coordinate_bounds: Tuple[int, int], row_range: Tuple[int, int], progress: bool):
    sensors_and_closest_beacons = [parse_line(line) for line in input_file]
    distance_to_beacon = {s: distance(s, b) for s, b in sensors_and_closest_beacons}

    search_column_range = ColumnRange(*coordinate_bounds)
    range_function = trange if progress else range
    for inspecting_row in range_function(row_range[0], row_range[1]+1):
        beacons_x_in_inspected_row = {bx for _, (bx, by) in sensors_and_closest_beacons if by == inspecting_row}
        sensors_x_in_inspected_row = {sx for (sx, sy), _ in sensors_and_closest_beacons if sy == inspecting_row}

        impossible_column_range_set = ColumnRangeSet()
        for x in chain(beacons_x_in_inspected_row, sensors_x_in_inspected_row):
            impossible_column_range_set.add(ColumnRange(x, x))
        
        for (sx, sy), sensor_distance_to_beacon in distance_to_beacon.items():
            y_distance = abs(inspecting_row - sy)
            discriminant = sensor_distance_to_beacon - y_distance
            if discriminant < 0:
                continue
            
            x_bound_a = sx - discriminant
            x_bound_b = sx + discriminant
            impossible_column_range_set.add(ColumnRange(x_bound_a, x_bound_b))
        
        for r in impossible_column_range_set.sorted_ranges:
            if r.intersects(search_column_range):
                if not r.is_superset_of(search_column_range):
                    print(impossible_column_range_set)
                    return


if __name__ == '__main__':
    main()
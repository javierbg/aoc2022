import numpy as np
from numpy.typing import NDArray


tail_position: NDArray[np.int64] = np.array([0, 0], dtype=np.int64)
head_position: NDArray[np.int64] = np.array([0, 0], dtype=np.int64)
tail_visited_positions = {(tail_position[0], tail_position[1])}

direction_step = dict(
    U=np.array([ 0, +1], dtype=np.int64),
    D=np.array([ 0, -1], dtype=np.int64),
    L=np.array([-1,  0], dtype=np.int64),
    R=np.array([+1,  0], dtype=np.int64)
)

while True:
    try:
        move = input().split()
        direction = move[0]
        distance = int(move[1])

        step = direction_step[direction]

        for _ in range(distance):
            original_head_position = head_position
            head_position = head_position + step
            head_tail_distance = np.abs(head_position - tail_position).max()
            if head_tail_distance <= 1:
                continue
            tail_position = original_head_position
            tail_visited_positions.add((tail_position[0], tail_position[1]))
    except EOFError:
        break

print(len(tail_visited_positions))
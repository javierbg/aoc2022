import numpy as np
from numpy.typing import NDArray
import time
import click


def draw_scene(knot_positions: list[NDArray[np.int64]], scene_size: tuple[int, int] = (60, 60)) -> None:
    scene = [['.' for _ in range(scene_size[1])] for _ in range(scene_size[0])]

    directions = np.array([1, -1], dtype=np.int64)
    scene_center = np.array([scene_size[1] // 2, scene_size[0] // 2])
    scene[scene_center[1]][scene_center[0]] = 's'

    for i, knot in reversed(list(enumerate(knot_positions))):
        knot_scene_pos = knot*directions + scene_center
        scene[knot_scene_pos[1]][knot_scene_pos[0]] = str(i)

    for row in scene:
        print(''.join(row), flush=False)
    print('', flush=True)


@click.command()
@click.argument('input_file', type=click.File('r'))
@click.option('-r', '--rate', default=None, type=int)
def main(input_file, rate):
    number_of_knots = 10
    knot_positions: list[NDArray[np.int64]] = [np.array([0, 0], dtype=np.int64) for _ in range(number_of_knots)]
    tail_visited_positions = {(knot_positions[-1][0], knot_positions[-1][1])}

    direction_step = dict(
        U=np.array([ 0, +1], dtype=np.int64),
        D=np.array([ 0, -1], dtype=np.int64),
        L=np.array([-1,  0], dtype=np.int64),
        R=np.array([+1,  0], dtype=np.int64)
    )

    for line in input_file:
        move = line[:-1].split()
        direction = move[0]
        distance = int(move[1])

        step = direction_step[direction]

        for _ in range(distance):
            knot_positions[0] = knot_positions[0] + step
            for head_index, tail_index in zip(range(number_of_knots-1), range(1, number_of_knots)):
                head_tail_distance_vector = knot_positions[head_index] - knot_positions[tail_index]
                assert (np.abs(head_tail_distance_vector) <= 2).all()
                head_tail_distance = np.abs(head_tail_distance_vector).max()
                if head_tail_distance > 1:
                    knot_positions[tail_index] = knot_positions[tail_index] + \
                        np.clip(head_tail_distance_vector, -1, 1)
            tail_visited_positions.add((knot_positions[-1][0], knot_positions[-1][1]))
            
            if rate is not None:
                draw_scene(knot_positions, (30, 60))
                if rate == 0:
                    input()
                else:
                    time.sleep(1 / rate)

    print(len(tail_visited_positions))


if __name__ == '__main__':
    main()
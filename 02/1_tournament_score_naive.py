from itertools import permutations

shape_score = dict(rock=1, paper=2, scissors=3)
outcome_score = dict(
    rock     = dict(rock=3, paper=6, scissors=0),
    paper    = dict(rock=0, paper=3, scissors=6),
    scissors = dict(rock=6, paper=0, scissors=3),
)

opponent_cipher = dict(A='rock', B='paper', C='scissors')
player_ciphers = [{alpha: 'rock', beta: 'paper', gamma: 'scissors'} for alpha, beta, gamma in permutations(['X', 'Y', 'Z'])]

total_scores = [0 for _ in player_ciphers]
while True:
    try:
        match = input()
        ciphered_opponent, ciphered_player = match.split()
        opponent = opponent_cipher[ciphered_opponent]

        for i, player_cipher in enumerate(player_ciphers):
            player = player_cipher[ciphered_player]
            total_scores[i] += shape_score[player]
            total_scores[i] += outcome_score[opponent][player]
    except EOFError:
        break

print(list(enumerate(player_ciphers)))
print(list(enumerate(total_scores)))
print(sorted(total_scores))
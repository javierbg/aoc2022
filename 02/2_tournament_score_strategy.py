shape_score = dict(rock=1, paper=2, scissors=3)
outcome_score = dict(
    rock     = dict(rock=3, paper=6, scissors=0),
    paper    = dict(rock=0, paper=3, scissors=6),
    scissors = dict(rock=6, paper=0, scissors=3),
)

opponent_shape_cipher = dict(A='rock', B='paper', C='scissors')
player_strategy_cipher = dict(X='lose', Y='draw', Z='win')

total_score = 0
while True:
    try:
        match = input()
        ciphered_opponent_shape, ciphered_player_strategy = match.split()
        opponent_shape = opponent_shape_cipher[ciphered_opponent_shape]
        player_strategy = player_strategy_cipher[ciphered_player_strategy]

        if player_strategy == 'draw':
            player_shape = opponent_shape
        elif player_strategy == 'win':
            player_shape = max(outcome_score[opponent_shape].items(), key=lambda e: e[1])[0]
        else:  # lose
            player_shape = min(outcome_score[opponent_shape].items(), key=lambda e: e[1])[0]

        print(f'Opponent will play {opponent_shape}. I need to {player_strategy}, so I play {player_shape}')

        total_score += shape_score[player_shape]
        total_score += outcome_score[opponent_shape][player_shape]
    except EOFError:
        break

print(total_score)
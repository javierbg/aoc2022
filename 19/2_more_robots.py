from dataclasses import dataclass
from functools import reduce
from math import ceil
import click
from typing import TextIO, Union, Literal, Optional
import re
import numpy as np
from operator import mul
from multiprocessing import Pool


@dataclass
class RobotCost:
    ore: int
    clay: int
    obsidian: int

    def __le__(self, other) -> bool:
        return (self.ore <= other.ore) and (self.clay <= other.clay) and (self.obsidian <= other.obsidian)

    def __sub__(self, other) -> 'RobotCost':
        return RobotCost(
            self.ore - other.ore,
            self.clay - other.clay,
            self.obsidian - other.obsidian
        )

@dataclass
class Blueprint:
    identifier: int
    ore_robot_cost: RobotCost
    clay_robot_cost: RobotCost
    obsidian_robot_cost: RobotCost
    geode_robot_cost: RobotCost


def parse_file(input_file: TextIO) -> list[Blueprint]:
    def parse_cost(cost: str) -> RobotCost:
        elements = map(lambda s: tuple(s.split()), cost.split(' and '))
        ore, clay, obsidian = 0, 0, 0
        for value, material in elements:
            value = int(value)
            if material == 'ore':
                ore += value
            elif material == 'clay':
                clay += value
            elif material == 'obsidian':
                obsidian += value
            else:
                raise RuntimeError()
            
        return RobotCost(ore, clay, obsidian)

    blueprints = list()
    r = re.compile(r'Blueprint (?P<id>\d+): Each ore robot costs (?P<ore_robot_cost>[^\.]+). Each clay robot costs (?P<clay_robot_cost>[^\.]+). Each obsidian robot costs (?P<obsidian_robot_cost>[^\.]+). Each geode robot costs (?P<geode_robot_cost>[^\.]+).')
    for line in input_file:
        m = r.match(line[:-1])
        assert m is not None
        blueprints.append(Blueprint(
            int(m.group('id')),
            parse_cost(m.group('ore_robot_cost')),
            parse_cost(m.group('clay_robot_cost')),
            parse_cost(m.group('obsidian_robot_cost')),
            parse_cost(m.group('geode_robot_cost')),
        ))
    return blueprints


@dataclass(eq=True, frozen=True)
class CollectingState:
    minutes_left: int = 32

    collected_ore: int = 0
    collected_clay: int = 0
    collected_obsidian: int = 0
    collected_geodes: int = 0

    n_ore_robots: int = 1
    n_clay_robots: int = 0
    n_obsidian_robots: int = 0
    n_geode_robots: int = 0

    def as_dict(self) -> dict[str, int]:
        return dict(
            minutes_left=self.minutes_left,

            collected_ore=self.collected_ore,
            collected_clay=self.collected_clay,
            collected_obsidian=self.collected_obsidian,
            collected_geodes=self.collected_geodes,

            n_ore_robots=self.n_ore_robots,
            n_clay_robots=self.n_clay_robots,
            n_obsidian_robots=self.n_obsidian_robots,
            n_geode_robots=self.n_geode_robots,
        )

    def projected_geodes(self) -> int:
        return self.collected_geodes + self.n_geode_robots * self.minutes_left

    def projected_best_geodes(self) -> int:
        return self.projected_geodes() + (self.minutes_left * (self.minutes_left + 1)) // 2

Action = Union[Literal['ore'], Literal['clay'], Literal['obsidian'], Literal['geode']]

def apply_action_if_feasible(blueprint: Blueprint, state: CollectingState, action: Action) -> Optional[CollectingState]:
    if action == 'ore':
        action_cost = blueprint.ore_robot_cost
    elif action == 'clay':
        action_cost = blueprint.clay_robot_cost
    elif action == 'obsidian':
        action_cost = blueprint.obsidian_robot_cost
    elif action == 'geode':
        action_cost = blueprint.geode_robot_cost

    available_materials = RobotCost(
        ore=state.collected_ore,
        clay=state.collected_clay,
        obsidian=state.collected_obsidian
    )
    collected_geodes = state.collected_geodes

    minutes_left = state.minutes_left

    if (action == 'ore') and (max(blueprint.ore_robot_cost.ore, blueprint.clay_robot_cost.ore, blueprint.obsidian_robot_cost.ore, blueprint.geode_robot_cost.ore) <= state.n_ore_robots):
        return None
    if (action == 'clay') and (max(blueprint.ore_robot_cost.clay, blueprint.clay_robot_cost.clay, blueprint.obsidian_robot_cost.clay, blueprint.geode_robot_cost.clay) <= state.n_clay_robots):
        return None
    if (action == 'obsidian') and (max(blueprint.ore_robot_cost.obsidian, blueprint.clay_robot_cost.obsidian, blueprint.obsidian_robot_cost.obsidian, blueprint.geode_robot_cost.obsidian) <= state.n_obsidian_robots):
        return None

    ore_wait = 0 if available_materials.ore >= action_cost.ore else \
        (np.inf if state.n_ore_robots == 0 else ceil((action_cost.ore - available_materials.ore) / state.n_ore_robots))
    clay_wait = 0 if available_materials.clay >= action_cost.clay else \
        (np.inf if state.n_clay_robots == 0 else ceil((action_cost.clay - available_materials.clay) / state.n_clay_robots))
    obsidian_wait = 0 if available_materials.obsidian >= action_cost.obsidian else \
        (np.inf if state.n_obsidian_robots == 0 else ceil((action_cost.obsidian - available_materials.obsidian) / state.n_obsidian_robots))

    minutes_ahead = max(ore_wait, clay_wait, obsidian_wait) + 1

    if minutes_ahead > minutes_left:
        return None
    assert isinstance(minutes_ahead, int)

    available_materials = RobotCost(
        ore = available_materials.ore + state.n_ore_robots * minutes_ahead,
        clay = available_materials.clay + state.n_clay_robots * minutes_ahead,
        obsidian = available_materials.obsidian + state.n_obsidian_robots * minutes_ahead,
    )
    available_materials = available_materials - action_cost
    collected_geodes += state.n_geode_robots * minutes_ahead
    minutes_left -= minutes_ahead

    return CollectingState(
        minutes_left,
        available_materials.ore,
        available_materials.clay,
        available_materials.obsidian,
        collected_geodes,
        state.n_ore_robots + (1 if action == 'ore' else 0),
        state.n_clay_robots + (1 if action == 'clay' else 0),
        state.n_obsidian_robots + (1 if action == 'obsidian' else 0),
        state.n_geode_robots + (1 if action == 'geode' else 0),
    )


def compute_maximum_geodes(blueprint: Blueprint) -> int:
    frontier: list[CollectingState] = [CollectingState()]

    most_collected_geodes = 0
    while frontier:
        candidate = frontier.pop(0)

        for action in ('ore', 'clay', 'obsidian', 'geode'):
            new_state = apply_action_if_feasible(blueprint, candidate, action)
            if new_state is None:
                continue

            if new_state.projected_geodes() > most_collected_geodes:
                most_collected_geodes = new_state.projected_geodes()

            if (new_state.minutes_left > 0) and (new_state.projected_best_geodes() > most_collected_geodes):
                frontier.append(new_state)

    return most_collected_geodes


def quality_level(blueprint: Blueprint) -> int:
    mg = compute_maximum_geodes(blueprint)
    return mg * blueprint.identifier
        

@click.command()
@click.argument('input_file', type=click.File('r'))
@click.option('-v', '--verbose', is_flag=True)
@click.option('-p', '--parallel', is_flag=True)
def main(input_file: TextIO, verbose: bool, parallel: bool):
    blueprints = parse_file(input_file)

    if parallel:
        pool = Pool(3)
        it = pool.map(compute_maximum_geodes, blueprints[:3])
    else:
        it = map(compute_maximum_geodes, blueprints[:3])

    result = 1
    for blueprint, maximum_geodes in zip(blueprints, it):
        print(blueprint.identifier, maximum_geodes)
        result *= maximum_geodes
        
    print(result)


if __name__ == '__main__':
    main()
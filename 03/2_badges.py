def item_priority(itemchar: str) -> int:
    order = ord(itemchar)
    if ord('a') <= order <= ord('z'):
        return order - ord('a') + 1
    else:
        return order - ord('A') + 27

total_priority = 0
while True:
    try:
        group_items = [set(input()) for _ in range(3)]
        coincident_items = set.intersection(*group_items)
        assert len(coincident_items) == 1
        total_priority += item_priority(coincident_items.pop())
    except EOFError:
        break

print(total_priority)
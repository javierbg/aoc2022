def item_priority(itemchar: str) -> int:
    order = ord(itemchar)
    if ord('a') <= order <= ord('z'):
        return order - ord('a') + 1
    else:
        return order - ord('A') + 27

total_priority = 0
while True:
    try:
        rucksack_contents = input()
        half_point = len(rucksack_contents)//2
        left_compartment, right_compartment = rucksack_contents[:half_point], rucksack_contents[half_point:]
        coincident_item = set(left_compartment) & set(right_compartment)
        assert len(coincident_item) == 1
        total_priority += item_priority(coincident_item.pop())
    except EOFError:
        break

print(total_priority)
import click
from typing import TextIO

def cycle_is_relevant(cycle: int) -> bool:
    return ((cycle - 20) % 40) == 0

@click.command()
@click.argument('input_file', type=click.File('r'))
def main(input_file: TextIO):
    x_register = 1
    executing_instruction = ['noop']
    instruction_cycles_left = 0
    cycle = 0
    accumulated_signal_strength = 0

    while (line := input_file.readline()):
        executing_instruction = line.split('\n')[0].split()

        if executing_instruction[0] == 'noop':
            instruction_cycles_left = 1
        elif executing_instruction[0] == 'addx':
            instruction_cycles_left = 2
        else:
            print(executing_instruction[0])
            raise NotImplementedError()

        while instruction_cycles_left:
            instruction_cycles_left -= 1
            cycle += 1
            
            if cycle_is_relevant(cycle):
                accumulated_signal_strength += cycle * x_register
                print(f'Cycle {cycle}: adding {cycle} * {x_register} = {cycle * x_register} [accumulated signal: {accumulated_signal_strength}]')

        if executing_instruction[0] == 'addx':
            x_register += int(executing_instruction[1])

    print(accumulated_signal_strength)

if __name__ == '__main__':
    main()
import click
from typing import TextIO

def draw_pixel(cycle: int, x_register: int) -> bool:
    return abs((x_register % 40) - ((cycle - 1) % 40)) <= 1

@click.command()
@click.argument('input_file', type=click.File('r'))
def main(input_file: TextIO):
    x_register = 1
    executing_instruction = ['noop']
    instruction_cycles_left = 0
    cycle = 0
    drawn_screen = ''

    while (line := input_file.readline()):
        executing_instruction = line.split('\n')[0].split()

        if executing_instruction[0] == 'noop':
            instruction_cycles_left = 1
        elif executing_instruction[0] == 'addx':
            instruction_cycles_left = 2
        else:
            print(executing_instruction[0])
            raise NotImplementedError()

        while instruction_cycles_left:
            instruction_cycles_left -= 1
            cycle += 1
            
            drawn_screen = drawn_screen + ('#' if draw_pixel(cycle, x_register) else ' ')

            if (cycle % 40) == 0:
                drawn_screen = drawn_screen + '\n'

        if executing_instruction[0] == 'addx':
            x_register += int(executing_instruction[1])

    print(drawn_screen)

if __name__ == '__main__':
    main()
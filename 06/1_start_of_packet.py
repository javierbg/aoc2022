from sys import stdin

window_size = 4
def main():
    char_queue = list()
    stream_index = 0
    buffer_index = 0
    while True:
        c = stdin.read(1)
        if not c:
            break
        if c.isspace():
            continue
        
        stream_index += 1
        if len(char_queue) < (window_size - 1):
            char_queue.append(c)
            continue
        elif len(char_queue) < window_size:
            char_queue.append(c)
        else:
            char_queue[buffer_index] = c
            buffer_index = (buffer_index + 1) % window_size

        assert len(char_queue) == window_size
        
        print(''.join(char_queue))

        if len(set(char_queue)) == window_size:
            print(stream_index)
            break


if __name__ == '__main__':
    main()
import click
from typing import TextIO, Tuple, Callable, Literal, Dict, List, Iterable
import numpy as np
from numpy.typing import NDArray
from itertools import product


Move = Literal['up', 'down', 'left', 'right']
Coord = Tuple[int, int]

def coord2index(field_shape: Coord) -> Tuple[Callable[[int, int], int], Callable[[int], Coord]]:
    n_cols = field_shape[1]

    def _coord2index(row: int, col: int) -> int:
        return col + row * n_cols

    def _index2col(idx: int) -> Coord:
        return (idx // n_cols, idx % n_cols)

    return _coord2index, _index2col


def valid_node(field_shape: Coord) -> Callable[[int, int], bool]:
    n_rows, n_cols = field_shape

    def _valid_node(row: int, col: int) -> bool:
        return (0 <= row < n_rows) and (0 <= col < n_cols)

    return _valid_node

def a_star(n_nodes: int, neighbours_of: Callable[[int], Iterable[int]], starts: List[int], goal: int, heuristic: Callable[[int], int]) -> List[int]:
    open_set = set(starts)
    came_from = np.full((n_nodes,), -1, dtype=int)
    current_best_cost_to = np.full((n_nodes,), np.inf, dtype=float)
    ideal_total_cost_from = np.full((n_nodes,), np.inf, dtype=float)
    for start in starts:
        current_best_cost_to[start] = 0.0
        ideal_total_cost_from[start] = heuristic(start)
    
    while open_set:
        current = min(((i, ideal_total_cost_from[i]) for i in open_set), key=lambda e: e[1])[0]

        if current == goal:
            path = [current]
            while current not in starts:
                current = came_from[current]
                path.append(current)
            return path
        
        open_set.remove(current)

        for neighbour in neighbours_of(current):
            possible_cost = current_best_cost_to[current] + 1
            if possible_cost < current_best_cost_to[neighbour]:
                came_from[neighbour] = current
                current_best_cost_to[neighbour] = possible_cost
                ideal_total_cost_from[neighbour] = possible_cost + heuristic(neighbour)
                open_set.add(neighbour)
    
    raise RuntimeError()


@click.command()
@click.argument('input_file', type=click.File('r'))
def main(input_file: TextIO):
    rows = list()
    start: Coord | None = None
    target: Coord | None = None
    for row_index, line in enumerate(input_file):
        line = line[:-1]
        if (start_column := line.find('S')) != -1:
            assert start is None
            start = (row_index, start_column)
        if (target_column := line.find('E')) != -1:
            assert target is None
            target = (row_index, target_column)
        
        line = line.replace('S', 'a').replace('E', 'z')
        rows.append(list(map(lambda c: ord(c) - 97, line)))
    assert start is not None
    assert target is not None

    field = np.array(rows)
    c2i, i2c = coord2index(field.shape)
    is_valid = valid_node(field.shape)
    n_nodes = field.shape[0] * field.shape[1]

    moves: Dict[Move, Coord] = {
        'up': (-1, 0),
        'down': (1, 0),
        'left': (0, -1),
        'right': (0, 1)
    }
    def can_move(src_row: int, src_col: int, move: Move) -> bool:
        row_move, col_move = moves[move]
        dst_row, dst_col = src_row + row_move, src_col + col_move
        if not is_valid(dst_row, dst_col):
            return False
        
        return (field[dst_row, dst_col] - field[src_row, src_col]) <= 1

    def neighbours_of(idx: int) -> Iterable[int]:
        src_row, src_col = i2c(idx)
        for move in moves:
            if can_move(src_row, src_col, move):
                row_move, col_move = moves[move]
                yield c2i(src_row + row_move, src_col + col_move)

    def heuristic(dst: int) -> Callable[[int], int]:
        dst_row, dst_col = i2c(dst)
        def _heuristic(src: int) -> int:
            src_row, src_col = i2c(src)
            return abs(src_row - dst_row) + abs(src_col - dst_col)
        return _heuristic
    
    starts = [c2i(r, c) for r, c in product(range(field.shape[0]), range(field.shape[1])) if field[r, c] == 0]
    shortest_path = a_star(n_nodes, neighbours_of, starts, c2i(*target), heuristic(c2i(*target)))

    print(len(shortest_path)-1)
    
    


if __name__ == '__main__':
    main()
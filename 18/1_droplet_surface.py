import click
from typing import TextIO, Tuple
import numpy as np
from numpy.typing import NDArray


Coord = Tuple[int, int, int]

def build_adjacency_matrix(lava_voxels_list: list[Coord]) -> NDArray:
    n_voxels = len(lava_voxels_list)
    adjacency_matrix = np.zeros((n_voxels, n_voxels), dtype=bool)
    for voxel_index, (vx, vy, vz) in enumerate(lava_voxels_list):
        for diff in (-1, 1):
            try:
                neighbour = (vx + diff, vy, vz)
                neighbour_index = lava_voxels_list.index(neighbour)
                adjacency_matrix[voxel_index, neighbour_index] = True
            except ValueError:
                pass

            try:
                neighbour = (vx, vy + diff, vz)
                neighbour_index = lava_voxels_list.index(neighbour)
                adjacency_matrix[voxel_index, neighbour_index] = True
            except ValueError:
                pass

            try:
                neighbour = (vx, vy, vz + diff)
                neighbour_index = lava_voxels_list.index(neighbour)
                adjacency_matrix[voxel_index, neighbour_index] = True
            except ValueError:
                pass
    return adjacency_matrix


@click.command()
@click.argument('input_file', type=click.File('r'))
def main(input_file: TextIO):
    lava_voxels_list: list[Coord] = list()
    for line in input_file:
        lava_voxels_list.append(tuple(map(int, line.strip().split(','))))

    n_voxels = len(lava_voxels_list)
    adjacency_matrix = build_adjacency_matrix(lava_voxels_list)

    n_edges = np.triu(adjacency_matrix).sum()
    n_exposed_sides = (6 * n_voxels) - 2 * n_edges
    
    print(n_exposed_sides)



if __name__ == '__main__':
    main()
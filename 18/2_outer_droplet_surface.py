import click
from typing import TextIO, Tuple
import numpy as np
from numpy.typing import NDArray


Coord = Tuple[int, int, int]

def build_adjacency_matrix(voxels_list: list[Coord]) -> NDArray:
    n_voxels = len(voxels_list)
    adjacency_matrix = np.zeros((n_voxels, n_voxels), dtype=bool)
    for voxel_index, (vx, vy, vz) in enumerate(voxels_list):
        for diff in (-1, 1):
            try:
                neighbour = (vx + diff, vy, vz)
                neighbour_index = voxels_list.index(neighbour)
                adjacency_matrix[voxel_index, neighbour_index] = True
            except ValueError:
                pass

            try:
                neighbour = (vx, vy + diff, vz)
                neighbour_index = voxels_list.index(neighbour)
                adjacency_matrix[voxel_index, neighbour_index] = True
            except ValueError:
                pass

            try:
                neighbour = (vx, vy, vz + diff)
                neighbour_index = voxels_list.index(neighbour)
                adjacency_matrix[voxel_index, neighbour_index] = True
            except ValueError:
                pass
    return adjacency_matrix


def steam_surface(
        lava_voxels_set: set[Coord],
        xmin: int,
        xmax: int,
        ymin: int,
        ymax: int,
        zmin: int,
        zmax: int
    ) -> list[Coord]:

    frontier = [(xmin, ymin, zmin)]
    steam_voxels: set[Coord] = set()

    def can_be_occupied(voxel: Coord) -> bool:
        vx, vy, vz = voxel
        if not ((xmin <= vx < xmax) and (ymin <= vy < ymax) and (zmin <= vz < zmax)):
            return False
        
        return (voxel not in lava_voxels_set) and (voxel not in steam_voxels)

    while frontier:
        new_voxel = frontier.pop()
        steam_voxels.add(new_voxel)
        vx, vy, vz = new_voxel

        for diff in (-1, 1):
            neighbours = [(vx+diff, vy, vz), (vx, vy+diff, vz), (vx, vy, vz+diff)]
            for neighbour in neighbours:
                if can_be_occupied(neighbour):
                    frontier.append(neighbour)

    assert (xmax-1, ymax-1, zmax-1) in steam_voxels
    return list(steam_voxels)



@click.command()
@click.argument('input_file', type=click.File('r'))
def main(input_file: TextIO):
    lava_voxels_list: list[Coord] = list()
    for line in input_file:
        lava_voxels_list.append(tuple(map(int, line.strip().split(','))))

    min_x = min(map(lambda e: e[0], lava_voxels_list))
    min_y = min(map(lambda e: e[1], lava_voxels_list))
    min_z = min(map(lambda e: e[2], lava_voxels_list))
    max_x = max(map(lambda e: e[0], lava_voxels_list))
    max_y = max(map(lambda e: e[1], lava_voxels_list))
    max_z = max(map(lambda e: e[2], lava_voxels_list))

    envolving_x_min = min_x - 1
    envolving_x_max = max_x + 2

    envolving_y_min = min_y - 1
    envolving_y_max = max_y + 2

    envolving_z_min = min_z - 1
    envolving_z_max = max_z + 2
    
    x_size = envolving_x_max - envolving_x_min
    y_size = envolving_y_max - envolving_y_min
    z_size = envolving_z_max - envolving_z_min

    steam_voxels_list = steam_surface(
        set(lava_voxels_list),
        envolving_x_min,
        envolving_x_max,
        envolving_y_min,
        envolving_y_max,
        envolving_z_min,
        envolving_z_max
    )

    n_voxels = len(steam_voxels_list)
    adjacency_matrix = build_adjacency_matrix(steam_voxels_list)

    n_edges = np.triu(adjacency_matrix).sum()
    n_exposed_sides = (6 * n_voxels) - 2 * n_edges
    n_outer_sides = (2* x_size * y_size) + (2 * x_size * z_size) + (2 * y_size * z_size)

    print(n_exposed_sides - n_outer_sides)



if __name__ == '__main__':
    main()
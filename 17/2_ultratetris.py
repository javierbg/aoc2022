import click
from typing import TextIO, Tuple, Hashable
import numpy as np
from numpy.typing import NDArray
from itertools import product

'''
Pieces:

....  ....  ....  #...  ....
....  .#..  ..#.  #...  ....
....  ###.  ..#.  #...  ##..
####  .#..  ###.  #...  ##..

'''

PIECES = [
    np.array([
        [False, False, False, False],
        [False, False, False, False],
        [False, False, False, False],
        [ True,  True,  True,  True],

    ]),
    np.array([
        [False, False, False, False],
        [False,  True, False, False],
        [ True,  True,  True, False],
        [False,  True, False, False],

    ]),
    np.array([
        [False, False, False, False],
        [False, False,  True, False],
        [False, False,  True, False],
        [True ,  True,  True, False],

    ]),
    np.array([
        [ True, False, False, False],
        [ True, False, False, False],
        [ True, False, False, False],
        [ True, False, False, False],

    ]),
    np.array([
        [False, False, False, False],
        [False, False, False, False],
        [ True,  True, False, False],
        [ True,  True, False, False],

    ]),
]

PIECE_WIDTH = [4, 3, 3, 1, 2]
PIECE_HEIGHT = [1, 3, 3, 4, 2]

def draw_field(field: NDArray, row_from: int, row_to: int):
    for row in range(row_from, row_to):
        print('|' + ''.join('#' if field[row, col] else '.' for col in range(7)) + '|')
    
    if row_to == field.shape[0]:
        print('+-------+')


def surface_hash(field: NDArray, surface_idx: int) -> Hashable:
    bitlist = field[surface_idx:surface_idx+7, :].flatten()
    return sum(2**i for i, b in enumerate(bitlist) if b)


def drop_piece(field: NDArray, wind_list: list[int], current_rocks_height: int, next_piece: int, current_wind: int, show_graphics: bool) -> Tuple[NDArray, int, int]:
    current_field_height = field.shape[0]
    if (current_rocks_height + 3 + 4) >= current_field_height:
        new_field = np.full((current_field_height*2, 7+3), False, dtype=bool)
        new_field[current_field_height:, :] = field
        field = new_field
        current_field_height = current_field_height * 2

    piece = PIECES[next_piece]
    piece_width = PIECE_WIDTH[next_piece]
    bottom_edge = current_field_height - (current_rocks_height + 3)
    left_edge = 2

    while True:
        # Try to push with wind
        next_left_edge = min(7 - piece_width, max(0, left_edge + wind_list[current_wind]))
        if not (field[bottom_edge-4:bottom_edge, next_left_edge:next_left_edge+4] & piece).any():
            left_edge = next_left_edge
        current_wind  = (current_wind + 1) % len(wind_list)

        # Try to push down
        next_bottom_edge = bottom_edge + 1
        if (next_bottom_edge > current_field_height) or \
            (field[next_bottom_edge-4:next_bottom_edge, left_edge:left_edge+4] & piece).any():
            # final piece position
            for i, j in product(range(4), range(4)):
                field[bottom_edge-(4-i), left_edge+j] = field[bottom_edge-(4-i), left_edge+j] or piece[i, j]

            if show_graphics:
                draw_field(field, current_field_height - 10, current_field_height)
                input()

            current_rocks_height = max(current_rocks_height, current_field_height - (bottom_edge - PIECE_HEIGHT[next_piece]))
            break
        else:
            bottom_edge = next_bottom_edge
    return field, current_rocks_height, current_wind

@click.command()
@click.argument('input_file', type=click.File('r'))
@click.option('-g', '--show-graphics', is_flag=True)
def main(input_file: TextIO, show_graphics: bool):
    wind_list = list(map(lambda e: -1 if e == '<' else 1, input_file.read()[:-1]))
    field = np.full((1024, 7+3), False, dtype=bool)
    current_rocks_height = 0
    current_wind = 0
    next_piece = 0
    total_simulation_steps = 1_000_000_000_000

    scenarios = dict()

    step = 0
    loop_found = False
    while True:
        if (current_rocks_height > 7) and (not loop_found):
            surface_idx = field.shape[0] - current_rocks_height
            scenario = (next_piece, current_wind, surface_hash(field, surface_idx))
            if scenario in scenarios:
                print(f'Found loop from step {scenarios[scenario][0]} to step {step}')
                loop_start_step = scenarios[scenario][0]
                loop_start_height = scenarios[scenario][1]
                loop_end_step = step
                loop_end_height = current_rocks_height
                loop_height_gain = loop_end_height - loop_start_height
                loop_length = loop_end_step - loop_start_step
                loop_found = True
                
                steps_left = (total_simulation_steps - loop_end_step) % loop_length
            else:
                scenarios[scenario] = (step, current_rocks_height)
        elif loop_found:
            steps_left -= 1
            if steps_left <= 0:
                break

        field, current_rocks_height, current_wind = drop_piece(field, wind_list, current_rocks_height, next_piece, current_wind, show_graphics)

        next_piece = (next_piece + 1) % len(PIECES)
        step += 1
    step -= 1
    total_height = current_rocks_height + (((total_simulation_steps - step) // loop_length) * loop_height_gain)
    print(total_height)


if __name__ == '__main__':
    main()
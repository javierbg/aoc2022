from typing import Iterable
import re

def process_input_row(row: str) -> Iterable[str | None]:
    assert (len(row) % 4) == 3
    for index in range(0, len(row), 4):
        if row[index:index+3] == '   ':
            yield None
        elif row[index] == '[' and row[index+2] == ']':
            yield row[index+1]
        else:
            raise ValueError

def main():
    n_stacks = 0
    rows = list()
    while True:
        line = input()
        try:
            row = list(process_input_row(line))
        except ValueError:
            break

        n_stacks = len(row)
        rows.append(row)
    input()  # ignore empty line

    print(rows)

    stacks = [list() for _ in range(n_stacks)]
    for stack_idx, stack in enumerate(stacks):
        for row in rows:
            if row[stack_idx] is not None:
                stack.append(row[stack_idx])
    
    # reverse so that pop() gets the top element and
    # append() puts a new element at the top
    stacks = list(map(list, (map(reversed, stacks))))

    while True:
        try:
            move = input()
            m = re.match(r'move (?P<n>\d+) from (?P<from>\d+) to (?P<to>\d+)', move)
            assert m is not None
            n_to_move = int(m.group('n'))
            move_from = int(m.group('from')) - 1
            move_to = int(m.group('to')) - 1
            
            print(f'Moving {n_to_move} crates from stack {move_from+1} to stack {move_to+1}')
            for _ in range(n_to_move):
                stacks[move_to].append(stacks[move_from].pop())
        except EOFError:
            break

    print(''.join(stack.pop() for stack in stacks))

if __name__ == '__main__':
    main()

import click
from typing import TextIO, Union, Callable
import yaml
from operator import add, sub, mul, floordiv


Monkey = int | str
Monkeys = dict[str, Monkey]

def parse_file(input_file: TextIO) -> Monkeys:
    return yaml.safe_load(input_file) 


def parse_or_not(v: str) -> str | int:
    try:
        return int(v)
    except ValueError:
        return v

OPERATOR = {
    '+': add,
    '-': sub,
    '*': mul,
    '/': floordiv,
}

def evaluate(monkeys: Monkeys, elem: str) -> Union[Callable[[int, int], int], list[int | str]]:
    if elem in OPERATOR:
        return OPERATOR[elem]
    
    parts = elem.split()
    if len(parts) == 1:
        return [monkeys[parts[0]]]

    return [parts[1], parts[0], parts[2]]

def evaluate_root(monkeys: Monkeys, verbose: bool) -> int:
    root = monkeys['root']
    if isinstance(root, int):
        return root
    
    root_parts = root.split()
    expression: list[int | str] = [root_parts[1], root_parts[0], root_parts[2]]
    stack: list[int] = list()

    while expression:
        if verbose:
            print(expression)
            print(stack)
            print()

        elem = expression.pop()

        if isinstance(elem, int):
            stack.append(elem)
            continue

        evaluated_elem = evaluate(monkeys, elem)

        if isinstance(evaluated_elem, Callable):
            operation = evaluated_elem
            operand1 = stack.pop()
            operand2 = stack.pop()
            stack.append(operation(operand1, operand2))
        else:
            expanded_expression = evaluated_elem
            expression.extend(expanded_expression)
    
    if verbose:
        print(expression)
        print(stack)
        print()

    return stack.pop()


@click.command()
@click.argument('input_file', type=click.File('r'))
@click.option('-v', '--verbose', is_flag=True)
def main(input_file: TextIO, verbose: bool):
    monkeys = parse_file(input_file)
    print(evaluate_root(monkeys, verbose))


if __name__ == '__main__':
    main()
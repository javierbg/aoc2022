import numpy as np

rows = list()
while True:
    try:
        rows.append(list(map(int, input())))
    except EOFError:
        break

matrix = np.array(rows)

visible_from_left = np.full_like(matrix, False, dtype=bool)
for row in range(matrix.shape[0]):
    highest_yet = -1
    for col in range(matrix.shape[1]):
        if matrix[row, col] > highest_yet:
            visible_from_left[row, col] = True
            highest_yet = matrix[row, col]

visible_from_right = np.full_like(matrix, False, dtype=bool)
for row in range(matrix.shape[0]):
    highest_yet = -1
    for col in range(matrix.shape[1]-1, -1, -1):
        if matrix[row, col] > highest_yet:
            visible_from_right[row, col] = True
            highest_yet = matrix[row, col]

visible_from_top = np.full_like(matrix, False, dtype=bool)
for col in range(matrix.shape[1]):
    highest_yet = -1
    for row in range(matrix.shape[0]):
        if matrix[row, col] > highest_yet:
            visible_from_top[row, col] = True
            highest_yet = matrix[row, col]

visible_from_bottom = np.full_like(matrix, False, dtype=bool)
for col in range(matrix.shape[1]):
    highest_yet = -1
    for row in range(matrix.shape[0]-1, -1, -1):
        if matrix[row, col] > highest_yet:
            visible_from_bottom[row, col] = True
            highest_yet = matrix[row, col]

visible_from_anywhere = visible_from_left | visible_from_right | visible_from_top | visible_from_bottom
print(visible_from_anywhere.sum())
import numpy as np

rows = list()
while True:
    try:
        rows.append(list(map(int, input())))
    except EOFError:
        break

matrix = np.array(rows)
best_scenic_score = 0
for row in range(matrix.shape[0]):
    for col in range(matrix.shape[1]):
        visible_up = 0
        visible_down = 0
        visible_left = 0
        visible_right = 0

        # look up
        look_col = col
        for look_row in range(row-1, -1, -1):
            if matrix[look_row, look_col] <= matrix[row, col]:
                visible_up += 1
            if matrix[look_row, look_col] == matrix[row, col]:
                break
        
        # look down
        look_col = col
        for look_row in range(row+1, matrix.shape[0]):
            if matrix[look_row, look_col] <= matrix[row, col]:
                visible_down += 1
            if matrix[look_row, look_col] == matrix[row, col]:
                break

        # look left
        look_row = row
        for look_col in range(col-1, -1, -1):
            if matrix[look_row, look_col] <= matrix[row, col]:
                visible_left += 1
            if matrix[look_row, look_col] == matrix[row, col]:
                break

        # look right
        look_row = row
        for look_col in range(col+1, matrix.shape[1]):
            if matrix[look_row, look_col] <= matrix[row, col]:
                visible_right += 1
            if matrix[look_row, look_col] == matrix[row, col]:
                break
        
        scenic_score = visible_up * visible_down * visible_left * visible_right
        best_scenic_score = max(scenic_score, best_scenic_score)

print(best_scenic_score)
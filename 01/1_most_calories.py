best_elf_calories = 0
current_elf_calories = 0

while True:
    try:
        snack_calories = input()
        if not snack_calories:
            best_elf_calories = max(best_elf_calories, current_elf_calories)
            current_elf_calories = 0
        else:
            current_elf_calories += int(snack_calories)
    except EOFError:
        break

print(best_elf_calories)
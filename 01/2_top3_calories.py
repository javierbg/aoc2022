k = 3
top_k_calories = [0 for _ in range(k)]
current_elf_calories = 0

while True:
    try:
        snack_calories = input()
        if not snack_calories:
            top_k_calories = sorted(top_k_calories + [current_elf_calories], reverse=True)[:3]
            current_elf_calories = 0
        else:
            current_elf_calories += int(snack_calories)
    except EOFError:
        break

print(top_k_calories)
print(sum(top_k_calories))
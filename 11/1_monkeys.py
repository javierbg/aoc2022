import click
from typing import TextIO, Callable, Iterable, Tuple
import re


class Monkey:
    def __init__(self, starting_items: list[int], operation: Callable[[int], int], divisibility_test: int, true_recipient: int, false_recipient) -> None:
        self.items = starting_items
        self.operation = operation
        self.divisibility_test = divisibility_test
        self.true_recipient = true_recipient
        self.false_recipient = false_recipient

        self.n_inspections = 0

    def inspect(self) -> Tuple[int, int]:
        self.n_inspections += 1

        item = self.items.pop(0)
        # print(f'  Monkey inspects an item with a worry level of {item}.')
        item = self.operation(item)
        item = item // 3

        if (item % self.divisibility_test) == 0:
            return (item, self.true_recipient)
        else:
            return (item, self.false_recipient)

    def is_done(self) -> bool:
        return not self.items

    def catch(self, item: int):
        self.items.append(item)

    def __str__(self) -> str:
        return f'Monkey(items={self.items}, operation={self.operation}, div_test={self.divisibility_test}, true_rec={self.true_recipient}, false_rec={self.false_recipient})'


def parse_monkeys(file: TextIO) -> Iterable[Monkey]:
    while True:
        m = re.match(r'Monkey \d+:$', file.readline())

        if not m:
            break

        m = re.match(r'Starting items: (?P<items>.+)$', file.readline().strip())
        assert m is not None
        starting_items = list(map(lambda s: int(s.strip()), m.group('items').split(',')))

        m = re.match(r'Operation: new = old (?P<operator>.) (?P<operand>\d+|old)$', file.readline().strip())
        assert m is not None
        operator = m.group('operator')
        if (operand := m.group('operand')) != 'old':
            operand = int(operand)

        def add_old():
            def _add_old(x):
                return x + x
            return _add_old
        
        def add_val(v):
            def _add_val(x):
                return x + v
            return _add_val

        def mul_old():
            def _mul_old(x):
                return x * x
            return _mul_old
        
        def mul_val(v):
            def _mul_val(x):
                return x * v
            return _mul_val

        if operand == 'old':
            if operator == '+':
                operation = add_old()
            else:
                operation = mul_old()
        else:
            if operator == '+':
                operation = add_val(operand)
            else:
                operation = mul_val(operand)

        m = re.match(r'Test: divisible by (?P<modulo>\d+)$', file.readline().strip())
        assert m is not None
        divisibility_test = int(m.group('modulo'))

        m = re.match(r'If true: throw to monkey (?P<true_recipient>\d+)$', file.readline().strip())
        assert m is not None
        true_recipient = int(m.group('true_recipient'))

        m = re.match(r'If false: throw to monkey (?P<false_recipient>\d+)$', file.readline().strip())
        assert m is not None
        false_recipient = int(m.group('false_recipient'))

        monkey = Monkey(starting_items, operation, divisibility_test, true_recipient, false_recipient)
        yield monkey
        
        file.readline()

@click.command()
@click.argument('input_file', type=click.File('r'))
def main(input_file: TextIO):
    monkeys = list(parse_monkeys(input_file))
    
    for round in range(20):
        for i, monkey in enumerate(monkeys):
            # print(f'Monkey {i}:')
            while not monkey.is_done():
                item, recipient = monkey.inspect()
                monkeys[recipient].catch(item)

    most_active_monkeys = sorted(monkeys, key=lambda m: m.n_inspections, reverse=True)[:2]
    business_level = most_active_monkeys[0].n_inspections * most_active_monkeys[1].n_inspections

    print(business_level)



if __name__ == '__main__':
    main()
from typing import Union, Optional, Iterable
from pprint import pprint
from functools import lru_cache


class DirTree:
    def __init__(self, parent: Union['DirTree', None] = None) -> None:
        self.tree: dict[str, Union['DirTree', int]] = dict()
        self.parent: Optional[DirTree] = parent
        self.size_value: Optional[int] = None

    def new_subdir(self, name: str) -> None:
        if name not in self.tree:
            self.tree[name] = DirTree(self)

    def new_file(self, name: str, size: int):
        if name in self.tree:
            assert self.tree[name] == size
        else:
            self.tree[name] = size

    def cd(self, name: str) -> 'DirTree':
        if name == '..':
            assert self.parent is not None
            return self.parent
        if name == '/':
            current_dir = self
            while current_dir.parent is not None:
                current_dir = current_dir.parent
            return current_dir

        if name not in self.tree:
            self.new_subdir(name)
        d = self.tree[name]
        assert isinstance(d, DirTree)
        return d

    def size(self) -> int:
        if self.size_value is None:
            self.size_value = sum(v if isinstance(v, int) else v.size() for v in self.tree.values())
        return self.size_value

    def traverse(self) -> Iterable['DirTree']:
        yield self
        for child in self.tree.values():
            if isinstance(child, DirTree):
                yield from child.traverse()
    
    def dict_tree(self):
        return {n: s if isinstance(s, int) else s.dict_tree() for n, s in self.tree.items()}

current_directory = DirTree()

while True:
    try:
        line = input()
        parts = line.split()
        if parts[0] == '$': # command
            args = parts[1:]
            if args[0] == 'cd':
                current_directory = current_directory.cd(args[1])
            elif args[0] == 'ls':
                pass
            else:
                raise NotImplementedError
        else: # file listing            
            if parts[0] == 'dir':
                current_directory.new_subdir(parts[1])
            else:
                size = int(parts[0])
                filename = parts[1]
                current_directory.new_file(filename, size)
    except EOFError:
        break

root_dir = current_directory.cd('/')
print(sum(dir.size() for dir in root_dir.traverse() if dir.size() <= 100000))

from dataclasses import dataclass
from functools import cache
import click
from typing import TextIO, Literal, Callable, Optional
from collections import defaultdict
from tqdm import tqdm


WALL = 1
OPEN = 0

Coord = tuple[int, int]
Direction = Literal['^', 'v', '<', '>']
Action = Optional[Direction]
BlizzardsState = dict[Coord, list[Direction]]

def parse_file(input_file: TextIO) -> tuple[int, int, Coord, Coord, BlizzardsState]:
    rows = [line[:-1] for line in input_file]
    field_height = len(rows) - 2
    field_width = len(rows[0]) - 2
    initial_position: Coord = (-1, [i for i, c in enumerate(rows[0]) if c == '.'][0] - 1)
    target_position: Coord = (field_height, [i for i, c in enumerate(rows[-1]) if c == '.'][0] - 1)
    blizzards: BlizzardsState = defaultdict(lambda: list())

    for r, row in enumerate(rows[1:-1]):
        for c, character in enumerate(row[1:-1]):
            if character in ('^', 'v', '<', '>'):
                blizzards[(r, c)].append(character)

    return field_height, field_width, initial_position, target_position, blizzards


@dataclass(eq=True, frozen=True)
class State:
    position: Coord
    minutes_elapsed: int


MOVES: dict[Direction, tuple[int, int]] = {
    '^': (-1, 0),
    'v': (1, 0),
    '<': (0, -1),
    '>': (0, 1),
}

def move(position: Coord, action: Action) -> Coord:
    if action is None:
        return position
    
    m = MOVES[action]
    return (
        position[0] + m[0],
        position[1] + m[1] 
    )


def move_is_valid(field_height: int, field_width: int, initial_position: Coord, target_position: Coord) -> Callable[[Coord, BlizzardsState], bool]:
    def _move_is_valid(new_pos: Coord, blizzards_state: BlizzardsState) -> bool:
        if (new_pos == initial_position) or (new_pos == target_position):
            return True

        r, c = new_pos
        if (r < 0) or (r >= field_height) or (c < 0) or (c >= field_width):
            return False

        return blizzards_state.get(new_pos, None) is None

    return _move_is_valid


def advance_blizzard_state(field_height: int, field_width: int) -> Callable[[BlizzardsState], BlizzardsState]:
    def _advance_blizzard_state(state: BlizzardsState) -> BlizzardsState:
        new_state: BlizzardsState = defaultdict(lambda: list())

        for pos, directions in state.items():
            for direction in directions:
                r, c = move(pos, direction)

                if r < 0:
                    new_pos = (field_height - 1, c)
                elif r >= field_height:
                    new_pos = (0, c)
                elif c < 0:
                    new_pos = (r, field_width - 1)
                elif c >= field_width:
                    new_pos = (r, 0)
                else:
                    new_pos = (r, c)
                
                new_state[new_pos].append(direction)

        return new_state

    return _advance_blizzard_state


def blizzard_state_at_time(initial_state: BlizzardsState, step: Callable[[BlizzardsState], BlizzardsState]) -> Callable[[int], BlizzardsState]:

    @cache
    def _blizzard_state_at_time(time: int) -> BlizzardsState:
        if time == 0:
            return initial_state
        
        return step(_blizzard_state_at_time(time - 1))

    return _blizzard_state_at_time


def distance(src: Coord, dst: Coord) -> int:
    return abs(dst[0] - src[0]) + abs(dst[1] - src[1])


def search_shortest_path(initial_position: Coord, target_position: Coord, blizzard_state_at: Callable[[int], BlizzardsState], valid_move: Callable[[Coord, BlizzardsState], bool]) -> int:
    frontier: dict[State, int] = {State(initial_position, 0): distance(initial_position, target_position)}

    visited: set[State] = set()

    best_time = float('inf')
    with tqdm() as t:
        while frontier:
            t.update()
            candidate, distance_to_target = min(frontier.items(), key=lambda e: e[1])
            del frontier[candidate]
            visited.add(candidate)

            if (candidate.position == target_position) and (candidate.minutes_elapsed < best_time):
                best_time = candidate.minutes_elapsed
                continue

            best_possible_time = candidate.minutes_elapsed + distance_to_target

            if best_possible_time >= best_time:
                continue

            next_blizzard_state = blizzard_state_at(candidate.minutes_elapsed + 1)

            for action in ('^', 'v', '<', '>', None):
                new_position = move(candidate.position, action)

                if not valid_move(new_position, next_blizzard_state):
                    continue
                
                new_state = State(new_position, candidate.minutes_elapsed + 1)
                if (new_state not in frontier) and (new_state not in visited):
                    frontier[new_state] = distance(new_position, target_position)

    assert isinstance(best_time, int)
    return best_time
    

@click.command()
@click.argument('input_file', type=click.File('r'))
def main(input_file: TextIO):
    field_height, field_width, initial_position, target_position, blizzards = parse_file(input_file)
    print(search_shortest_path(
        initial_position,
        target_position,
        blizzard_state_at_time(blizzards, advance_blizzard_state(field_height, field_width)),
        move_is_valid(field_height, field_width, initial_position, target_position)
    ))


if __name__ == '__main__':
    main()
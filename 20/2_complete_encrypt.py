import click
from typing import Callable, TextIO, Iterator


DECRYPTING_KEY = 811589153

class Node:
    next: 'Node'
    prev: 'Node'

    def __init__(self, v: int) -> None:
        self.v = v

    def __str__(self) -> str:
        return str(self.v)

    def __repr__(self) -> str:
        return str(self)
    

class CircularLinkedList:
    def __init__(self, l: list[int]) -> None:
        self.node_list = list(map(Node, l))
        for node, next in zip(self.node_list[:-1], self.node_list[1:]):
            node.next = next
            next.prev = node
        
        self.node_list[-1].next = self.node_list[0]
        self.node_list[0].prev = self.node_list[-1]

        for i, node in enumerate(self.node_list):
            assert node.next is self.node_list[(i+1) % len(self.node_list)]
            assert node.prev is self.node_list[(i-1) % len(self.node_list)]

    def __len__(self) -> int:
        return len(self.node_list)

    def __iter__(self) -> Iterator[Node]:
        current_node = self.node_list[0]
        while True:
            yield current_node
            current_node = current_node.next

    def __getitem__(self, index: int) -> Node:
        return self.node_list[index]

    def __str__(self) -> str:
        current_node = self.node_list[0]
        s = f'[{current_node}'
        while current_node.next is not self.node_list[0]:
            current_node = current_node.next
            s += f', {current_node}'
        s += ']'
        return s

    def apply(self, f: Callable[[Node], None]):
        current_node = self.node_list[0]
        f(current_node)
        current_node = current_node.next
        while current_node is not self.node_list[0]:
            f(current_node)
            current_node = current_node.next


def parse_file(input_file: TextIO) -> CircularLinkedList:
    return CircularLinkedList(list(map(int, input_file)))


def move_number(number_list: CircularLinkedList, index: int):
    node_to_move = number_list[index]

    move_after_node = node_to_move

    if node_to_move.v > 0:
        for _ in range(node_to_move.v % (len(number_list)-1)):
            move_after_node = move_after_node.next
    elif node_to_move.v < 0:
        move_after_node = move_after_node.prev
        for _ in range(abs(node_to_move.v) % (len(number_list)-1)):
            move_after_node = move_after_node.prev
    else:
        return

    if move_after_node.next is node_to_move:
        return
    if move_after_node is node_to_move:
        move_after_node = move_after_node.next
    
    node_to_move.prev.next = node_to_move.next
    node_to_move.next.prev = node_to_move.prev

    node_to_move.next = move_after_node.next
    node_to_move.prev = move_after_node

    node_to_move.prev.next = node_to_move
    node_to_move.next.prev = node_to_move


def extract_coordinates(number_list: CircularLinkedList) -> int:
    zero_node = None
    for node in number_list:
        if node.v == 0:
            zero_node = node
            break

    assert zero_node is not None

    current_node = zero_node
    acc = 0
    for i in range(1, 3000+1):
        current_node = current_node.next

        if (i % 1000) == 0:
            print(f'Number {i // 1000} is {current_node.v}')
            acc += current_node.v

    return acc


def mix(number_list: CircularLinkedList, verbose: bool):
    if verbose:
        print(number_list)
    for i in range(len(number_list)):
        move_number(number_list, i)
        if verbose:
            print(number_list)


@click.command()
@click.argument('input_file', type=click.File('r'))
@click.option('-v', '--verbose', is_flag=True)
def main(input_file: TextIO, verbose: bool):
    number_list = parse_file(input_file)
    
    def decrypt_mul(n: Node):
        n.v = n.v * DECRYPTING_KEY

    number_list.apply(decrypt_mul)
    for _ in range(10):
        mix(number_list, verbose)

    print(extract_coordinates(number_list))

if __name__ == '__main__':
    main()
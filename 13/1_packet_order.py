import abc
import click
from typing import TextIO
from abc import ABC
from itertools import zip_longest
from ast import literal_eval


def grouper(iterable, n, fillvalue=None):
    # From itertools recipes
    "Collect data into non-overlapping fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


class Packet(ABC):
    @abc.abstractmethod
    def __lt__(self, other) -> bool:
        ...

    @abc.abstractmethod
    def __str__(self) -> str:
        ...

    def __repr__(self) -> str:
        return str(self)

class ItemList(Packet):
    def __init__(self, items: list[Packet]) -> None:
        self.items = items

    def __str__(self):
        return '[' + ', '.join(str(i) for i in self.items) + ']'

    def __lt__(self, other) -> bool:
        if isinstance(other, ItemInt):
            return self < ItemList([other])
        elif isinstance(other, ItemList):
            for s, o in zip(self.items, other.items):
                in_order = s < o
                out_of_order = o < s
                equal = not (in_order or out_of_order)
                if equal:
                    continue
                if in_order:
                    return True
                return False
            return len(self.items) < len(other.items)
        
        raise RuntimeError()

class ItemInt(Packet):
    def __init__(self, val: int) -> None:
        self.val = val

    def __str__(self) -> str:
        return str(self.val)

    def __lt__(self, other) -> bool:
        if isinstance(other, ItemInt):
            return self.val < other.val
        elif isinstance(other, ItemList):
            return ItemList([self]) < other
        
        raise RuntimeError()

ProtoItem = int | list['ProtoItem']

def to_packet(il: ProtoItem) -> Packet:
    if isinstance(il, int):
        return ItemInt(il)
    else:
        items = [to_packet(i) for i in il]
        return ItemList(items)

@click.command()
@click.argument('input_file', type=click.File('r'))
def main(input_file: TextIO):
    acc_index = 0
    for pair_index, (packet1, packet2, _) in enumerate(grouper(input_file, 3), start=1):  # type: ignore
        left_packet, right_packet = to_packet(literal_eval(packet1)), to_packet(literal_eval(packet2))

        if left_packet < right_packet:
            print(f'Packet pair {pair_index} is ordered')
            acc_index += pair_index
    
    print(acc_index)


if __name__ == '__main__':
    main()
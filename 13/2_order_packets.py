import abc
import click
from typing import TextIO
from abc import ABC
from itertools import zip_longest
from pprint import pprint
from ast import literal_eval


def grouper(iterable, n, fillvalue=None):
    # From itertools recipes
    "Collect data into non-overlapping fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


class Packet(ABC):
    @abc.abstractmethod
    def __lt__(self, other) -> bool:
        ...

    @abc.abstractmethod
    def __str__(self) -> str:
        ...

    def __repr__(self) -> str:
        return str(self)

class ItemList(Packet):
    def __init__(self, items: list[Packet]) -> None:
        self.items = items

    def __str__(self):
        return '[' + ', '.join(str(i) for i in self.items) + ']'

    def __lt__(self, other) -> bool:
        if isinstance(other, ItemInt):
            return self < ItemList([other])
        elif isinstance(other, ItemList):
            for s, o in zip(self.items, other.items):
                in_order = s < o
                out_of_order = o < s
                equal = not (in_order or out_of_order)
                if equal:
                    continue
                if in_order:
                    return True
                return False
            return len(self.items) < len(other.items)
        
        raise RuntimeError()

class ItemInt(Packet):
    def __init__(self, val: int) -> None:
        self.val = val

    def __str__(self) -> str:
        return str(self.val)

    def __lt__(self, other) -> bool:
        if isinstance(other, ItemInt):
            return self.val < other.val
        elif isinstance(other, ItemList):
            return ItemList([self]) < other
        
        raise RuntimeError()

ProtoItem = int | list['ProtoItem']

def to_packet(il: ProtoItem) -> Packet:
    if isinstance(il, int):
        return ItemInt(il)
    else:
        items = [to_packet(i) for i in il]
        return ItemList(items)

@click.command()
@click.argument('input_file', type=click.File('r'))
def main(input_file: TextIO):
    packets = [to_packet(literal_eval(line.strip())) for line in input_file if line.strip()]

    divider_packets = [to_packet([[2]]), to_packet([[6]])]
    packets.extend(divider_packets)
    sorted_packets = sorted(packets)

    decoder_key = 1
    for packet_index, packet in enumerate(sorted_packets, start=1):
        if packet in divider_packets:
            decoder_key *= packet_index

    pprint(packets)
    print('')
    pprint(sorted_packets)
    print(decoder_key)
    

if __name__ == '__main__':
    main()
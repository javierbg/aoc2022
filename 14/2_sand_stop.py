import click
from typing import TextIO, Tuple, Iterable
import numpy as np
from numpy.typing import NDArray
from itertools import product
import matplotlib as mpl
mpl.use('tkagg')
from matplotlib.figure import Figure
from matplotlib.axes import Axes
from matplotlib.image import AxesImage
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.artist import Artist


EMPTY = 0
ROCK = 1
SAND = 2
SOURCE = 3
SAND_SOURCE = (500, 0)
SAND_SOURCE_X = SAND_SOURCE[0]
SAND_SOURCE_Y = SAND_SOURCE[1]

COLORMAP = mpl.colors.LinearSegmentedColormap.from_list('cave', [
    (0.0, 0.0, 0.0, 1.0),  # empty
    (1.0, 1.0, 1.0, 1.0),  # rock
    (1.0, 0.0, 0.0, 1.0),  # sand
    (0.0, 1.0, 0.0, 1.0),  # source
], 4)


def show_cave(cave: NDArray) -> Tuple[Figure, Axes, AxesImage]:
    fig, ax = plt.subplots(1, 1)
    img = ax.imshow(cave, COLORMAP)
    return fig, ax, img


def drop_sand(cave: NDArray) -> bool:
    if cave[SAND_SOURCE_Y, SAND_SOURCE_X] == SAND:
        return False

    sand_x, sand_y = SAND_SOURCE_X, SAND_SOURCE_Y

    while True:
        if (sand_y + 1) >= cave.shape[0]:
            break

        if (sand_x == 0) or ((sand_x +1) >= cave.shape[1]):
            raise RuntimeError()

        below_x, below_y = sand_x, sand_y + 1
        if cave[below_y, below_x] == EMPTY:
            sand_x, sand_y = below_x, below_y
            continue

        left_x, left_y = sand_x - 1, sand_y + 1
        if cave[left_y, left_x] == EMPTY:
            sand_x, sand_y = left_x, left_y
            continue

        right_x, right_y = sand_x + 1, sand_y + 1
        if cave[right_y, right_x] == EMPTY:
            sand_x, sand_y = right_x, right_y
            continue
        break

    cave[sand_y, sand_x] = SAND
    return True



@click.command()
@click.argument('input_file', type=click.File('r'))
@click.option('-g', '--show-graphics', is_flag=True)
def main(input_file: TextIO, show_graphics: bool):
    sand_paths = list()
    for line in input_file:
        str_coords = line.strip().split(' -> ')
        coords = [tuple(map(int, s.split(','))) for s in str_coords]
        sand_paths.append(coords)

    min_col = min(min(c[0] for c in cs) for cs in sand_paths)
    min_row = min(min(c[1] for c in cs) for cs in sand_paths)
    max_col = max(max(c[0] for c in cs) for cs in sand_paths)
    max_row = max(max(c[1] for c in cs) for cs in sand_paths)

    assert min_col >= 0
    assert min_row >= 0

    cave_size = (max_row+2, SAND_SOURCE_X*2)
    cave = np.full(cave_size, EMPTY, dtype=int)
    cave[SAND_SOURCE_Y, SAND_SOURCE_X] = SOURCE

    for path in sand_paths:
        for (prev_col, prev_row), (next_col, next_row) in zip(path[:-1], path[1:]):
            if prev_col == next_col:
                # vertical line
                for row in range(min(prev_row, next_row), max(prev_row, next_row)+1):
                    cave[row, prev_col] = ROCK
            elif prev_row == next_row:
                # horizontal line
                for col in range(min(prev_col, next_col), max(prev_col, next_col)+1):
                    cave[prev_row, col] = ROCK
            else:
                raise RuntimeError()

    if show_graphics:
        drawing_slice_rows = slice(0, max_row + 2)
        drawing_slice_cols = slice(min_col-10, max_col+10)
        fig, ax, img = show_cave(cave[drawing_slice_rows, drawing_slice_cols])

        
        def update_data() -> Iterable[int]:
            sand_dropped = 0
            while drop_sand(cave):
                sand_dropped += 1
                yield sand_dropped
            print(sand_dropped)

        def update_image(frame, *fargs) -> Iterable[Artist]:
            img.set_data(cave[drawing_slice_rows, drawing_slice_cols])
            return [img]

        anim = FuncAnimation(fig, update_image, update_data(), interval=1, blit=True)

        plt.show()
    else:
        dropped_sand = 0
        while drop_sand(cave):
            dropped_sand += 1
        print(dropped_sand)


if __name__ == '__main__':
    main()